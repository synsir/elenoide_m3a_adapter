#pragma once

#include <synhal.h>

#include "../stmbase/drivers/mcp4201/mcp42010.h"

// setup and measure input PWM
// set valuess to output digital potentiometers
class PwmMeasurer : public syn::Thread
{
  // the timer clock is divided by this number + 1
  // the timers run at the base clock of the processor, so 84 MHz
  // dividing by 4 leads to 1 / 21 microsec per clock tick
  static const uint16_t TIMER_PRSC = 3;
  // this is the number at which the timer rolls over and generates an update event
  // so, 52500 * (1 / 21) mmicrosec will lead to a 2500 microsec per reload
  // we use this value, as the sensor runs at 3889 to 4392 microseconds
  // this way we absolutely avoid missing a sensor tick, as they can't run at the same speed
  // the boundaries are 2500us * 1 = 2500 and 2500us * 2 = 5000
  static const uint16_t TIMER_RELOAD = 52500;

  struct captures_t
  {
    // one timer has 4 capture registers, 2 registers make up one channel
    // in the final buffer arrangement, maps to capture register 1 and 2
    uint16_t channel_0_on;
    uint16_t channel_0_off;
    // in the final buffer arrangement, maps to capture register 3 and 4
    uint16_t channel_1_on;
    uint16_t channel_1_off;
  };

  class PwmBuffer
  {
    static const uint16_t BUFFER_ENTRY_COUNT = 4;
    static const uint16_t BUFFER_COUNT = 2;

  public:
    // setup the timer and dma to cyclic capture of the pwm
    void init(uint16_t timer_num, uint16_t dma_num, uint16_t dma_chan, bool mapping);

    void setStorePointer(float* pstore_0, float* pstore_1)
    {
      _store[0] = pstore_0;
      _store[1] = pstore_1;
    }

    void storeValue(uint16_t channel, float value)
    {
      *_store[channel] = value;
    }

    // swap the readbuffer beeing used for calculation
    void set_read_buffer(uint16_t number);

    // calculate the period and the duty cycle
    void calculate(uint16_t channel, int32_t &period, int32_t &duty);

  private:
    void _read_time_change(uint16_t previous, uint16_t *readptr, int32_t &t_1, int32_t &t_2);
    // this variable will be set by swapping the read / write buffer
    // it points to the buffer that is finished and can be used for calculation
    captures_t *_readbuffer;
    // pointer to the output storages for each channel
    float* _store[2];
    // preserve the last capture of the previous buffer
    captures_t *_previous;
    // the double buffer is to be populated by dma requests in circular mode
    captures_t _buffer_0[BUFFER_ENTRY_COUNT];
    captures_t _buffer_1[BUFFER_ENTRY_COUNT];
  };

public:
  PwmMeasurer();
  void run();

  // called by the final dma interrupt to notify this thread and
  // advance the double buffer accordingly
  static void dma_half_done();
  static void dma_full_done();

private:
  // this signal is set by the dma routine whenever writing a buffer
  // is finished and the variables needed for calculation are updated
  static syn::Signal _dma_sig;
  static const uint16_t NUM_PWM_BUFFERS = 5;
  // these are the individual PWM buffers, one for each timer
  // one buffer serves 2 channels
  static PwmBuffer _buffers[NUM_PWM_BUFFERS];
  // the potentiometer daisychain
  static Mcp42010<NUM_PWM_BUFFERS> _potis;
};

extern syn::Timer timer_1;
extern syn::Timer timer_2;
