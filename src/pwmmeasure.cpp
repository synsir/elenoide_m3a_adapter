#include "pwmmeasure.h"
#include "usbsend.h"

PwmMeasurer::PwmBuffer PwmMeasurer::_buffers[PwmMeasurer::NUM_PWM_BUFFERS];
syn::Signal PwmMeasurer::_dma_sig;
Mcp42010<PwmMeasurer::NUM_PWM_BUFFERS> PwmMeasurer::_potis;

using namespace syn;

PwmMeasurer::PwmMeasurer() : Thread("pwmmeasure", SYN_OS_PRIO_HIGH + 1, 512)
{
}

void PwmMeasurer::run()
{
  syn::Led led;
  uint16_t led_toggle_count = 0;
  // initialize dma Signal
  // it will be triggered once by the last timer in the chain
  // when this happens, all transfers for one buffer are complete
  // the reapointer stored in the PwmBuffer and the previous values will be set
  _dma_sig.init();

  // initialize Extis and Timers to capture pwm. after initialization, they immediatelly run
  // and they will basically be synchronized, since they run at the same speed and same clock
  // the first one to be initialized, will be the first to trigger a DMA request and save its Data.
  // Therefore, the order of initialization will be with the lowest priority dma last.
  // This ensures that, when the last DMA sends an interrupt, all the others are done too.
  // when this happens, all transfers for one buffer are complete
  // the reapointer stored in the PwmBuffer and the previous values will be set

  auto pwrite = UsbSend::pulsereport.position;

  // timer 5, dma 0, channel 6
  _buffers[0].init(5, 0, 6, true);
  // channel 5 & 7
  _buffers[0].setStorePointer(pwrite + 4, pwrite + 6);

  // timer 2, dma 1, channel 3
  _buffers[1].init(2, 1, 3, false);
  // channel 1 & 6
  _buffers[1].setStorePointer(pwrite + 0, pwrite + 5);

  // timer 3, dma 2, channel 5
  _buffers[2].init(3, 2, 5, true);
  // channel 4 & 10
  _buffers[2].setStorePointer(pwrite + 3, pwrite + 9);

  // timer 4, dma 6, channel 2
  _buffers[3].init(4, 6, 2, false);
  // channel 3 & 2
  _buffers[3].setStorePointer(pwrite + 2, pwrite + 1);

  // timer 1, dma 2:5, channel 6
  _buffers[4].init(1, 8+5, 6, false);
  // channel 9 & 8
  _buffers[4].setStorePointer(pwrite + 8, pwrite + 7);

  // enable IRQ only for the last DMA that will trigger after all others are done
  {
    Dma dma;
    dma.init(8+5);
    dma.enableIrq(Dma::IRQ_STATUS_FULL | Dma::IRQ_STATUS_HALF);
  }

  // initialize the potentiometers
  _potis.init(1, true, true);

  // wait for signal to populate buffer
  // buffer 0 will likely be incomplete
  // so we start the first read using buffer 1
  _dma_sig.wait();
  while (true)
  {
    if(++led_toggle_count == 10)
    {
      led_toggle_count = 0;
      led.toggle();
    }
    // when the signal was set, the the buffer is refreshed
    _dma_sig.wait();
    UsbSend::pulsereport.stamp = syn::System::milliseconds();
    // calculate all the channels
    for (auto pbuf = _buffers; pbuf != &_buffers[NUM_PWM_BUFFERS]; ++pbuf)
    {
      int32_t period, duty;
      float result;

      pbuf->calculate(0, period, duty);
      result = float(duty) / float(period);
      if(result > 0.0f && result < 1.0f)
      {
        pbuf->storeValue(0, result);
      }
      pbuf->calculate(1, period, duty);
      result = float(duty) / float(period);
      if(result > 0.0f && result < 1.0f)
      {
        pbuf->storeValue(1, result);
      }
    }
    // use the calculated values to update the digital potentiometers
    _potis.setf(UsbSend::pulsereport.position, NUM_PWM_BUFFERS * 2);
  }
}

// setup the timer and dma to cyclic capture of the pwm
void PwmMeasurer::PwmBuffer::init(uint16_t timer_num, uint16_t dma_num, uint16_t dma_chan, bool mapping)
{
  _readbuffer = _buffer_0; // set the readbuffer to a sensible value
  Timer tim;
  tim.init(timer_num);
#ifdef DEBUG
  tim.stopForDebug();
#endif
  Dma dma;
  dma.init(dma_num);
  // one buffer entry consists of 2 channels a 2 int16, so multiply buffer size by 4 to get transfer count
  auto readaddress = tim.enableDmaUpdate(13, 4);
  dma.cyclicP2M(readaddress, (uint16_t *)&_buffer_0, BUFFER_COUNT * BUFFER_ENTRY_COUNT * 4);
  dma.enableBurstMode(Dma::Burst_of_4_beats, Dma::Burst_of_4_beats, Dma::FIFO_HALF);
  dma.start(dma_chan);
  tim.configPwmCapture(TIMER_PRSC, TIMER_RELOAD, Timer::Clk_4, mapping); // timer starts running
  
  switch (timer_num)
  {
  case 1:
      tim.enableInput('A', 8, false, false);
      tim.enableInput('A', 10, false, false);
    break;
  case 2:
      tim.enableInput('A', 0, false, false);
      tim.enableInput('A', 2, false, false);
    break;
  case 3:
      tim.enableInput('B', 5, false, false);
      tim.enableInput('B', 1, false, false);
    break;
  case 4:
      tim.enableInput('B', 6, false, false);
      tim.enableInput('B', 8, false, false);
    break;
  case 5:
      tim.enableInput('A', 1, false, false);
      tim.enableInput('A', 3, false, false);
    break;
  
  default:
    break;
  }
}

// swap the readbuffer beeing used for calculation
void PwmMeasurer::PwmBuffer::set_read_buffer(uint16_t number)
{
  _previous = _readbuffer + BUFFER_ENTRY_COUNT - 1;
  _readbuffer = _buffer_0 + (number * BUFFER_ENTRY_COUNT);
}

void PwmMeasurer::PwmBuffer::_read_time_change(uint16_t previous, uint16_t* readptr, int32_t& t_1, int32_t &t_2)
{
  uint16_t *endptr = readptr + (sizeof(captures_t) / sizeof(uint16_t)) * BUFFER_ENTRY_COUNT;
  int32_t time = 0;
  for (; readptr != endptr; readptr += (sizeof(captures_t) / sizeof(uint16_t)))
  {
    if (previous != *readptr)
    {
      previous = *readptr;
      t_1 = time + *readptr;
      break;
    }
    time += TIMER_RELOAD;
  }
  for (; readptr != endptr; readptr += (sizeof(captures_t) / sizeof(uint16_t)))
  {
    if (previous != *readptr)
    {
      t_2 = time + *readptr;
      break;
    }
    time += TIMER_RELOAD;
  }
} 

// calculate the cycle and on time of the selected channel
void PwmMeasurer::PwmBuffer::calculate(uint16_t channel, int32_t &period, int32_t &duty)
{
  uint16_t prev_val;
  uint16_t *readptr;
  int32_t t_on_1, t_on_2;
  t_on_1 = t_on_2 = -1;
  if (channel == 0)
  {
    prev_val = _previous->channel_0_on;
    readptr = &_readbuffer->channel_0_on;
  }
  else
  {
    prev_val = _previous->channel_1_on;
    readptr = &_readbuffer->channel_1_on;
  }
  _read_time_change(prev_val, readptr, t_on_1, t_on_2);
  
  int32_t t_off_1, t_off_2;
  t_off_1 = t_off_2 = -1;
  if (channel == 0)
  {
    prev_val = _previous->channel_0_off;
    readptr = &_readbuffer->channel_0_off;
  }
  else
  {
    prev_val = _previous->channel_1_off;
    readptr = &_readbuffer->channel_1_off;
  }
  _read_time_change(prev_val, readptr, t_off_1, t_off_2);

  //OS_ASSERT(t_on_1 != 0xFFFF && t_on_2 != 0xFFFF, ERR_FORBIDDEN);
  //OS_ASSERT(t_off_1 != 0xFFFF && t_off_2 != 0xFFFF, ERR_FORBIDDEN);
  if(t_on_2 > 0 && t_off_2 > 0)
  {
    // period t_on_2 - t_on_1
    period = t_on_2 - t_on_1;
    // calculate the on time
    if (t_off_1 > t_on_1)
    {
      duty = t_off_1 - t_on_1;
    }
    else if(t_off_2 < t_on_2)
    {
      duty = t_off_2 - t_on_1;
    }
    else // t_off_2 > t_on_2
    {
      duty = t_off_2 - t_on_2;
    }
  }
  else
  {
    period = 1;
    duty = -1;
  }
}


void PwmMeasurer::dma_half_done()
{
  PwmBuffer *pbuf = _buffers;
  PwmBuffer *end = &_buffers[NUM_PWM_BUFFERS];
  for (; pbuf != end; ++pbuf)
  {
    pbuf->set_read_buffer(0);
  }
  _dma_sig.set();
}

void PwmMeasurer::dma_full_done()
{
  PwmBuffer *pbuf = _buffers;
  PwmBuffer *end = &_buffers[NUM_PWM_BUFFERS];
  for (; pbuf != end; ++pbuf)
  {
    pbuf->set_read_buffer(1);
  }
  _dma_sig.set();
}