
#include "synrpc_usbcon.h"

/* Here come the handler definitions. They will be evoked with the message in an internal buffer
 * and on the PacketHandler Thread, thus any RTOS function calls are allowed.
 * The packet reception continues in the background to another buffer. If you stall too long, it
 * will start to stall the Host.
 * Since the programmer is responsible for keeping align requirements, please be careful!
 * Also, there will be a check of the packets checksum before it gets here, An error will be send automatically
 * in case of mismatched messages.
 * Furthermore, returning anything else other than 0 will be regarded as an error and interpreted as
 * a pointer to a zero terminated C-String. This string will be copied into the internal Error-Message buffer.
 * It allows only a maximum size of 26 characters. Anything more will be discarded. You have been warned.
 */
/// USER INCLUDES ///
#include "pwmmeasure.h"
/// USER INCLUDES END ///

/* PulseReportMsg message definition
* # time of capture in milliseconds since system start
* uint32 stamp
* # the measured position of the sensor
* float position[10]
*/
const char* syn::PulseReportHandler(const syn::PulseReportMsg& msg){
  (void)msg; // avoid "unused variable" compiler warning
/// USER CODE PulseReportMsg ///
/// END USER CODE ///
  return 0;
}

