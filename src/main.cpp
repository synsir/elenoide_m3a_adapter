#include <synhal.h>

#include "synrpc_usbcon.h"
syn::UsbRpc::Handler packethandler;

#include "pwmmeasure.h"
PwmMeasurer pwmmeasure;

#include "usbsend.h"
UsbSend usbsend;

int main (void)
{
  syn::System::init();

  packethandler.start();
  pwmmeasure.start();
  usbsend.start();

  /* Start scheduler */
  syn::System::spin();
  /* We should never get here as control is now taken by the scheduler */
  for (;;)
    ;
}
