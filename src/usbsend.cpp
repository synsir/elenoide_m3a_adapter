#include "usbsend.h"

syn::PulseReportMsg UsbSend::pulsereport;
syn::PulseReportMsg pulsereport_local_copy;

UsbSend::UsbSend() : Thread("usbsend", SYN_OS_PRIO_HIGH, 512)
{
}

void UsbSend::run() {
  
  syn::Rate<100> rate;
  while (true){
    rate.sleep();

    {
      syn::Region r;
      pulsereport_local_copy = pulsereport;
    }
    
    syn::UsbRpc::Handler::sendMessage(pulsereport_local_copy, 5);
  }
}
