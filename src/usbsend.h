#pragma once

#include <synhal.h>
#include "synrpc_usbcon.h"

// send measured values over usb
class UsbSend : public syn::Thread
{
public:
  UsbSend();
  void run();

  static syn::PulseReportMsg pulsereport;
};
