elenoide_m3a_adapter

## modem manager

the kernel thinks because of the cdc usb class the device might accept serial commands.
but it doesn't, that stuff messes with the synrpc (not really, its robust)

remove it from the computer entirely to be safe.

`sudo apt --purge remove modemmanager`
