import synrpc_usbcon as usb
from time import sleep
import random
import time

current_milli_time = lambda: int(round(time.time() * 1000))
current_micro_time = lambda: int(round(time.time()))
import sys

ph = usb.PacketHandler()

start = 0
counter = 0

def myhndl(msg):
    global start
    global counter
    counter += 1
    if counter == 1:
        counter = 0
        pos = [round(x * 255) for x in msg.position]
        print(msg.stamp - start, pos)
        #print(msg.stamp - start, list(msg.position))
        start = msg.stamp

ph.addMessageHandler(usb.PulseReportMsg, myhndl)

ph.start()

try:
    while True:
        sleep(1)
except:
    pass

ph.shutdown()

